package com.example.chenyan_xjy
import android.os.Bundle
import androidx.activity.enableEdgeToEdge
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.RecyclerView
import androidx.viewpager2.widget.ViewPager2


class MainActivity : AppCompatActivity() {
    private var viewPager: ViewPager2? = null
    private var newsRecyclerView: RecyclerView? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        enableEdgeToEdge()
        setContentView(R.layout.activity_main)
        viewPager = findViewById<ViewPager2>(R.id.viewPager)
        // 设置适配器等..
        // 初始化新闻列表
        newsRecyclerView = findViewById<RecyclerView>(R.id.newsRecyclerView)
        // 设置适配器等...

    }



}

